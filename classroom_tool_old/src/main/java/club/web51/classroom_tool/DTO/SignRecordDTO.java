package club.web51.classroom_tool.DTO;

import club.web51.classroom_tool.DO.dataDO.SignRecord;
import org.springframework.beans.BeanUtils;

import java.util.Date;

/**
 * @Classname signRecordDTO
 * @Description TODO
 * @Created by yln
 */
public class SignRecordDTO extends SignRecord {
    private Boolean isvalid;

    public Boolean getIsvalid() {
        return isvalid;
    }

    public void setIsvalid(Boolean isvalid) {
        this.isvalid = isvalid;
    }

    public SignRecordDTO(SignRecord signRecord){
        BeanUtils.copyProperties(signRecord,this);
        if(getDeadline().before(new Date())){
            setIsvalid(true);
        }else{
            setIsvalid(false);
        }
    }
}
