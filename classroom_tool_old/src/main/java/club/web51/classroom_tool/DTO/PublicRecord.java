package club.web51.classroom_tool.DTO;

import club.web51.classroom_tool.DO.baseDO.basePeople;
import club.web51.classroom_tool.DO.baseDO.baseSystemRecord;
import club.web51.classroom_tool.DO.roleDO.Teacher;

import java.util.List;

/**
 * @Classname Record
 * @Description TODO 查询符合条件类的返回实体类
 * @Date 2020/10/29 16:23
 * @Created by HanYuHao
 */
public class PublicRecord{
    /**
     * 记录发起教师id
     */
    public Long id;
    /**
     * 记录列表
     */
    public List<? extends baseSystemRecord> recordList;
    /**
     * 用户列表
     */
    public List<? extends basePeople> userList;

    public PublicRecord() {

    }

    public PublicRecord(List<? extends baseSystemRecord> recordList, List<? extends basePeople> userList) {
        this.recordList = recordList;
        this.userList = userList;
    }

    public List<? extends baseSystemRecord> getRecordList() {
        return recordList;
    }

    public void setRecordList(List<? extends baseSystemRecord> recordList) {
        this.recordList = recordList;
    }

    public List<? extends basePeople> getUserList() {
        return userList;
    }

    public void setUserList(List<? extends basePeople> userList) {
        this.userList = userList;
    }
}
