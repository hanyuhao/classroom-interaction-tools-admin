package club.web51.classroom_tool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClassroomToolApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClassroomToolApplication.class, args);
    }

}
