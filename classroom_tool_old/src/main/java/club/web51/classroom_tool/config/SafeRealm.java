package club.web51.classroom_tool.Config;

import club.web51.classroom_tool.DO.roleDO.Student;
import club.web51.classroom_tool.DO.roleDO.Teacher;
import club.web51.classroom_tool.DTO.User;
import club.web51.classroom_tool.Repositories.StudentRepository;
import club.web51.classroom_tool.Repositories.TeacherRepository;
import club.web51.classroom_tool.Utils.ConvertUtil;
import club.web51.classroom_tool.Utils.SFWhere;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * @Classname SafeRealm
 * @Description TODO
 * @Date 2020/10/28 19:27
 * @Created by HanYuHao
 */
public class SafeRealm extends AuthorizingRealm {
    @Resource
    private StudentRepository studentRepository;
    @Resource
    private TeacherRepository teacherRepository;

    /**
     * 由于系统暂时用不到RBAC模型，只做基于用户类型的权限处理，暂时不重写
     * @param principalCollection
     * @return null
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        /**
         * 获取Token中的用户信息,未登录返回空
         */
        UserAuthenticationToken userauthenticationToken = (UserAuthenticationToken) authenticationToken;

        String password = ConvertUtil.getToString(authenticationToken.getCredentials());
        if(Objects.isNull(password)){
            return null;
        }
        Student student = new Student();
        Teacher teacher = new Teacher();

        if((userauthenticationToken).getType().equals(User.STUDENT)){

            student.setUsername(userauthenticationToken.getUsername());
            student.setPassword(ConvertUtil.getToString(userauthenticationToken.getCredentials()));
//            student.setTelephone(user.getTelephone());

            Optional<Student> student1 = studentRepository.findOne(SFWhere.and(student).build());
            if(!student1.isPresent()){
                return null;
            }
            student = student1.get();
        }else if(userauthenticationToken.getType().equals(User.TEACHER)){

            teacher.setUsername(userauthenticationToken.getUsername());
            teacher.setPassword(ConvertUtil.getToString(userauthenticationToken.getCredentials()));
//            teacher.setTelephone(user.getTelephone());

            Optional<Teacher>teacher1 =  teacherRepository.findOne(SFWhere.and(teacher).build());
            if(!teacher1.isPresent()){
                return null;
            }
            teacher = teacher1.get();
        }

        /**
         *  如果登录了，设置返回认证的信息
         */

        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo();
        if(userauthenticationToken.getType().equals(User.TEACHER)){
            authenticationInfo.setCredentials(ConvertUtil.setToChar_(teacher.getPassword()));
            authenticationInfo.setPrincipals(new SimplePrincipalCollection(teacher, this.getName()));
        }
           // authenticationInfo.setCredentials(teacher);
        if(userauthenticationToken.getType().equals(User.STUDENT)){
            authenticationInfo.setCredentials(ConvertUtil.setToChar_(student.getPassword()));
            authenticationInfo.setPrincipals(new SimplePrincipalCollection(student, this.getName()));
        }

          //  authenticationInfo.setCredentials(student);
        return authenticationInfo;
    }


    /**
     * 使得系统支持自定义Token
     * @param token
     * @return
     */

    @Override
    public boolean supports(AuthenticationToken token){
        return token instanceof UserAuthenticationToken;
    }
}
