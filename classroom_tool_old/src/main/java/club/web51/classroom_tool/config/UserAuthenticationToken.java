package club.web51.classroom_tool.Config;

import club.web51.classroom_tool.DTO.User;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * @Classname UserAuthenticationToken
 * @Description 用户Token类,暂时用不到授权，简化了
 * @Date 2020/10/28 20:55
 * @Created by HanYuHao
 */
public class UserAuthenticationToken extends UsernamePasswordToken {

    private Integer type;

    public UserAuthenticationToken(User user) {
        super(user.getUsername(), user.getPassword());
        this.type = user.getType();
    }

    public Integer getType() {
        return type;
    }
}
