package club.web51.classroom_tool.Controllers.RecordController;

import club.web51.classroom_tool.DO.dataDO.SignRecord;
import club.web51.classroom_tool.DO.sysDO.Msg;
import club.web51.classroom_tool.DTO.LoginUser;
import club.web51.classroom_tool.Enums.MsgEnum;
import club.web51.classroom_tool.Exceptions.ErrDataException;
import club.web51.classroom_tool.Repositories.SignRecordRepository;
import club.web51.classroom_tool.Utils.MsgUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.data.domain.Pageable;
import club.web51.classroom_tool.DO.dataDO.SignRecord_Data;
import club.web51.classroom_tool.Services.SignRecord_DataService;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 服务类
 * @author Hanyuhao
 * @since 1.0.0
 */
@Api(value = "签到记录模块",tags={"签到记录操作接口"})
@RestController
@CrossOrigin
@RequestMapping("/signRecord_Data")
public class SignRecord_DataController {

    final
    SignRecord_DataService signRecord_DataService;
    @Resource
    SignRecordRepository signRecordRepository;

    public SignRecord_DataController(SignRecord_DataService signRecord_DataService) {
        this.signRecord_DataService = signRecord_DataService;
    }

    /**
     * 根据SignRecord_Data的字段,自动生成条件,字段的值为null不生成条件
     * http://localhost:8080/user/?id=1
     * @param signRecord_Data 实体对象
     * @param pageable 分页/排序对象
     * @return 返回的是实体,里面涵盖分页信息及状态码
     */
    @ApiOperation("动态查询，可输入对应对象的一个或多个属性")
     @GetMapping
    @RequiresPermissions(logical = Logical.OR, value = {"1","2","3"})
     ResponseEntity<Object> search(SignRecord_Data signRecord_Data, Pageable pageable) {
             return signRecord_DataService.search(signRecord_Data, pageable);
     }
     @PostMapping
     @ApiOperation("增加一个签到记录")
     @RequiresPermissions(logical = Logical.OR, value = {"1"})
     Msg create(SignRecord_Data signRecord_data) {
        signRecord_data.setId(null);
        signRecord_DataService.updateOne(signRecord_data);
        return MsgUtil.success();
     }
     @PutMapping
     @ApiOperation("修改一个签到记录")
     @RequiresPermissions(logical = Logical.OR, value = {"3"})
     void update(SignRecord_Data signRecord_data){
        signRecord_DataService.updateOne(signRecord_data);
     }

    @RequiresPermissions(logical = Logical.OR, value = {"2","3"})
    @ApiOperation("删除一个签到记录")
     @DeleteMapping("/")
     void delete(Long id){
        signRecord_DataService.deleteOne(id);
     }
}
