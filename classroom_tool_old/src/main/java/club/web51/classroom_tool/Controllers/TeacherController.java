package club.web51.classroom_tool.Controllers;

import club.web51.classroom_tool.DO.collDO.Academy;
import club.web51.classroom_tool.DO.roleDO.Teacher;
import club.web51.classroom_tool.DO.sysDO.Msg;
import club.web51.classroom_tool.Enums.MsgEnum;
import club.web51.classroom_tool.Enums.UserTypeEnum;
import club.web51.classroom_tool.Exceptions.ErrDataException;
import club.web51.classroom_tool.Services.TeacherService;
import club.web51.classroom_tool.Utils.MsgUtil;
import club.web51.classroom_tool.Utils.ObjectUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Objects;

/**
 * 服务类
 * @author HanYuhao
 * @since 1.0.0
 */
@RestController
@CrossOrigin
@RequestMapping("/teacher")
@Api(value = "老师模块",tags={"老师操作接口"})

public class TeacherController {

    final
    TeacherService teacherService;

    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    /**
     * 根据Teacher的字段,自动生成条件,字段的值为null不生成条件
     * http://localhost:8080/user/?id=1
     * @param teacher 实体对象
     * @param pageable 分页/排序对象
     * @return 返回的是实体,里面涵盖分页信息及状态码
     */
     @GetMapping
     @ApiOperation("动态查询，可输入对应对象的一个或多个属性")
     @RequiresPermissions(logical = Logical.OR, value = {"2","3"})
     ResponseEntity<Object> search(Teacher teacher, Pageable pageable) {
            Subject currentUser = SecurityUtils.getSubject();
            return teacherService.search(teacher, pageable);
     }
    @PostMapping
    @ApiOperation("创建老师")
    @RequiresPermissions(value = {"3"})
    Msg create(@RequestBody @Valid Teacher teacher, BindingResult bindingResult) {
         if(bindingResult.hasErrors()){
             throw new ErrDataException(Objects.requireNonNull(bindingResult.getFieldError()).getDefaultMessage());
         }
        teacher.setId(null);
        teacherService.updateOne(teacher);
        return MsgUtil.success();
    }
    @PutMapping
    @ApiOperation("修改老师")
    @RequiresPermissions(logical = Logical.OR, value = {"2", "3"})
    Msg update(@Valid Teacher teacher,BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            throw new ErrDataException(Objects.requireNonNull(bindingResult.getFieldError()).getDefaultMessage());
        }
        if(ObjectUtil.isNull(teacher.getId()))
            throw new ErrDataException(MsgEnum.ERROR_DATA_EMPTY);
        teacherService.updateOne(teacher);
        return MsgUtil.success();
    }
    @DeleteMapping("/")
    @ApiOperation("删除老师，输入ID")
    @RequiresPermissions(value = {"3"})
    Msg delete(Long id){
        teacherService.deleteOne(id);
        return MsgUtil.success();
    }
}
