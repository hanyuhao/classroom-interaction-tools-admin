package club.web51.classroom_tool.Controllers;

import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.data.domain.Pageable;
import club.web51.classroom_tool.DO.roleDO.Admin;
import club.web51.classroom_tool.Services.AdminService;
/**
 * 服务类
 * @author Hanyuhao
 * @since 1.0.0
 */
@RestController
@CrossOrigin
@RequestMapping("/admin")
@Api(value = "管理员模块",tags={"管理员操作接口"})
public class AdminController {

    final
    AdminService adminService;

    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    /**
     * 根据Admin的字段,自动生成条件,字段的值为null不生成条件
     * http://localhost:8080/user/?id=1
     * @param admin 实体对象
     * @param pageable 分页/排序对象
     * @return 返回的是实体,里面涵盖分页信息及状态码
     */
     @GetMapping
     @RequiresPermissions(logical = Logical.OR, value = {"3"})
     ResponseEntity<Object> search(Admin admin, Pageable pageable) {
             return adminService.search(admin, pageable);
     }
     @PostMapping
     void create() {

     }
     @PutMapping
     void update(){
     }
     @DeleteMapping("/")
     void delete(){
     }
}
