package club.web51.classroom_tool.DO.collDO;

import club.web51.classroom_tool.DO.baseDO.baseObject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @Classname Group
 * @Description TODO 组别/班级
 * @Created by cxk
 */
@Entity
public class Group extends baseObject {
    @Id
    @GeneratedValue
    private Long id;
    /**
     * 组/班 名称
     */
    @Column
    private String name;
    /**
     * 容量
     */
    @Column
    private Integer capacity;
}
