package club.web51.classroom_tool.DO.sysDO;

import club.web51.classroom_tool.Enums.MsgEnum;

/**
 * @Classname Msg
 * @Description TODO
 * @Created by yln
 */
public class Msg {
    /**
     * 状态码
     */
    private Integer code;
    /**
     * 消息描述
     */
    private String description;
    /**
     * 实体结果，通常是DTO层数据
     */
    private Object note;

    public Msg() {

    }

    public Msg(MsgEnum msgEnum) {
        this.code = msgEnum.getCode();
        this.description = msgEnum.getDescription();
    }

    public Msg(MsgEnum msgEnum,Object note) {
        this.code = msgEnum.getCode();
        this.description = msgEnum.getDescription();
        this.note = note;
    }

    public Msg(Integer code, String description, Object note) {
        this.code = code;
        this.description = description;
        this.note = note;
    }

    public Integer getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public Object getNote() {
        return note;
    }
}
