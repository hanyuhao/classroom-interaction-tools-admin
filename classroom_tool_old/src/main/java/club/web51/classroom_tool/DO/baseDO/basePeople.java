package club.web51.classroom_tool.DO.baseDO;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;


/**
 * @Classname basePeople
 * @Description TODO
 * @Date 2020/10/28 20:21
 * @Created by cxk
 */
@MappedSuperclass
public class basePeople {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String name;
    @Column
    private Long telephone;
    @Column
    private String sex;
    @Column(unique = true)
    private String username;
    @Column
    private String password;
    @Column
    private String location;
    @Column
    private String salt;

    public Long getTelephone() {
        return telephone;
    }

    public void setTelephone(Long telephone) {
        this.telephone = telephone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
}

