package club.web51.classroom_tool.DO.dataDO;

import club.web51.classroom_tool.DO.baseDO.baseSystemRecord;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @Classname SignRecord
 * @Description TODO 签到记录
 * @Created by yln
 */
@Entity
public class SignRecord extends baseSystemRecord {
    /**
     * 截止时间
     */
    @Column
    @NotNull(message = "截止日期不能为空")
    private Date deadline;
    /**
     * 所属班级ID
     */
    @Column
    private Long clazz_id;
    /**
     * 签到类型
     */
    @Column
    @NotNull(message = "记录类型不能为空")
    private Integer type;

    public SignRecord() {

    }

    public SignRecord(Date deadline, Long clazz_id, Integer type) {
        this.deadline = deadline;
        this.clazz_id = clazz_id;
        this.type = type;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Long getClazz_id() {
        return clazz_id;
    }

    public void setClazz_id(Long clazz_id) {
        this.clazz_id = clazz_id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
