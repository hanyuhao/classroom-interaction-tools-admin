package club.web51.classroom_tool.DO.roleDO;

import club.web51.classroom_tool.DO.baseDO.basePeople;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

/**
 * @Classname Student
 * @Description TODO
 * @Created by yln
 */
@Entity
public class Student extends basePeople {
    @Column
    @GeneratedValue
    private Long sid;
    @Column
    private Long academy_id;
    @Column
    private Long major_id;
    @Column
    private Long clazz_id;

    public Long getSid() {
        return sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    public Long getAcademy_id() {
        return academy_id;
    }

    public void setAcademy_id(Long academy_id) {
        this.academy_id = academy_id;
    }

    public Long getMajor_id() {
        return major_id;
    }

    public void setMajor_id(Long major_id) {
        this.major_id = major_id;
    }

    public Long getClazz_id() {
        return clazz_id;
    }

    public void setClazz_id(Long clazz_id) {
        this.clazz_id = clazz_id;
    }
}
