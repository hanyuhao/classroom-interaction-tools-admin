package club.web51.classroom_tool.DO.baseDO;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

/**
 * @Classname baseSystemRecords
 * @Description TODO
 * @Date 2020/10/28 22:18
 * @Created by cxk
 */
@MappedSuperclass
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
public class baseSystemRecord {
    @Id
    @GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
    private String bid;
    //记录类型ID
    @Column
    private Long typeId;
    //记录显示名
    @Column
    private String name;
    //注意事项
    @Column
    private String note;
    /**
     * 创建时间
     */
    @CreationTimestamp
    private Date CreateTime;
    /**
     * 修改时间
     */
    @UpdateTimestamp
    private Date UpdateTime;

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date createTime) {
        CreateTime = createTime;
    }

    public Date getUpdateTime() {
        return UpdateTime;
    }

    public void setUpdateTime(Date updateTime) {
        UpdateTime = updateTime;
    }
}
