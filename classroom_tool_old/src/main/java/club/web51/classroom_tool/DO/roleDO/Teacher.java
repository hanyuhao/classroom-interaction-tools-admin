package club.web51.classroom_tool.DO.roleDO;

import club.web51.classroom_tool.DO.baseDO.basePeople;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

/**
 * @Classname Teacher
 * @Description TODO
 * @Created by yln
 */
@Entity
public class Teacher extends basePeople {
    @Column
    @GeneratedValue
    private Long tid;

    public Long getTid() {
        return tid;
    }

    public void setTid(Long tid) {
        this.tid = tid;
    }
}
