package club.web51.classroom_tool.Repositories;

import club.web51.classroom_tool.DO.dataDO.Group_Clazz;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * 服务类
 * @author wxt
 * @since 1.0.0
 */
@Repository
public interface Group_ClazzRepository extends JpaRepository<Group_Clazz,Long>,JpaSpecificationExecutor<Group_Clazz>{
}