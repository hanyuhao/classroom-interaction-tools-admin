package club.web51.classroom_tool.Repositories;

import club.web51.classroom_tool.DO.collDO.Clazz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * 服务类
 * @author wxt
 * @since 1.0.0
 */
@Repository
public interface ClazzRepository extends JpaRepository<Clazz,Long>,JpaSpecificationExecutor<Clazz>{

}