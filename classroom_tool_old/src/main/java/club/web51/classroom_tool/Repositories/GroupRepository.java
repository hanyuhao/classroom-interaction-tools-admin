package club.web51.classroom_tool.Repositories;

import club.web51.classroom_tool.DO.collDO.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * 服务类
 * @author wxt
 * @since 1.0.0
 */
@Repository
public interface GroupRepository extends JpaRepository<Group,Long>,JpaSpecificationExecutor<Group>{

}