package club.web51.classroom_tool.Repositories;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import club.web51.classroom_tool.DO.roleDO.Student;

/**
 * 服务类
 * @author wxt
 * @since 1.0.0
 */
@Repository
public interface StudentRepository extends JpaRepository<Student,Long>,JpaSpecificationExecutor<Student>{


}