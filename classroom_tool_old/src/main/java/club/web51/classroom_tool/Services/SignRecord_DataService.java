package club.web51.classroom_tool.Services;

import club.web51.classroom_tool.DO.dataDO.SignRecord;
import club.web51.classroom_tool.DO.sysDO.Msg;
import club.web51.classroom_tool.DTO.LoginUser;
import club.web51.classroom_tool.Enums.MsgEnum;
import club.web51.classroom_tool.Exceptions.ErrDataException;
import club.web51.classroom_tool.Repositories.SignRecordRepository;
import club.web51.classroom_tool.Utils.BeanPlusUtils;
import club.web51.classroom_tool.Utils.MsgUtil;
import club.web51.classroom_tool.Utils.ObjectUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired ; 
import club.web51.classroom_tool.Repositories.SignRecord_DataRepository;
import club.web51.classroom_tool.Utils.SFWhere;
import club.web51.classroom_tool.DO.dataDO.SignRecord_Data;

import java.util.Date;
import java.util.Optional;

/**
 * 服务类
 * @author yln
 * @since 1.0.0
 */
@Service
public class SignRecord_DataService {
    @Autowired
    SignRecord_DataRepository signRecord_DataRepository;
    @Autowired
    SignRecordRepository signRecordRepository;
    /**
     * 根据SignRecord_Data的字段自动生成条件,字段值为null不生成条件
     * 如果是数值型的字段,前端不传入值,默认是0,例如ID的类型是Long,如果不传值,默认是0
     * 可以自己设置下SFWhere.and(signRecord_Data).equal(实体.getId()>0,"id",实体.getId()).build()
     * @param signRecord_Data     实体对象
     * @param pageable 分页对象
     * @return 返回分页\状态码
     */
    public ResponseEntity<Object> search(SignRecord_Data signRecord_Data, Pageable pageable) {
        Page<SignRecord_Data> all = signRecord_DataRepository.findAll(SFWhere.and(signRecord_Data)
                //.equal(user.getId() > 0, "id", user.getId())
                //.in(true, "userName", longs)
                //.equal(!字段值的判断.equals("") && 字段值的判断 != null, "字段名称", 字段值)
//                .like(signRecord_Data.getName() != null, "name", "%" + academy.getName() + "%")
                //....
                .build(), pageable);
        return new ResponseEntity<>(all, HttpStatus.OK);
    }
    public Msg updateOne(SignRecord_Data signRecord_data){
        if(ObjectUtil.isNull(signRecord_data)){
            throw new ErrDataException(MsgEnum.ERROR_DATA_EMPTY);
        }
        if(ObjectUtil.isNull(signRecord_data.getBid())){
            throw new ErrDataException(MsgEnum.ERROR_DATA_EXCEPT);
        }
        Optional<SignRecord> signRecordOptional = signRecordRepository.findByBid(signRecord_data.getBid());
        if(!signRecordOptional.isPresent()){
            throw new ErrDataException(MsgEnum.ERROR_DATA_EXCEPT);
        }
        SignRecord signRecord = signRecordOptional.get();
        //通过签到数据判断记录是否合法
        if(signRecord.getDeadline().before(new Date())){
            throw new ErrDataException(MsgEnum.ERROR_NO_ROOT_SIGN);
        }
        Boolean is_signed = signRecord_DataRepository.existsByBidAndSid(signRecord_data.getBid(),signRecord_data.getSid());
        if(is_signed){
            throw new ErrDataException(MsgEnum.ERROR_ALREADY_SIGN);
        }

        if(ObjectUtil.isNull(signRecord_data.getId())){
            signRecord_DataRepository.save(signRecord_data);
            return MsgUtil.success();
        }
        SignRecord_Data signRecord_data1 = signRecord_DataRepository.getOne(signRecord_data.getId());
        BeanPlusUtils.copyPropertiesIgnoreNull(signRecord_data,signRecord_data1);
        signRecord_DataRepository.save(signRecord_data1);
        return MsgUtil.success();
    }

    public Msg deleteOne(Long id){
        if(ObjectUtil.isNull(id)){
            throw new ErrDataException(MsgEnum.ERROR_DATA_EMPTY);
        }
        signRecord_DataRepository.deleteById(id);
        return MsgUtil.success();
    }
}
