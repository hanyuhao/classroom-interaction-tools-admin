package club.web51.classroom_tool.Utils;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * @Classname ProfileUtil
 * @Description TODO
 * @Date 2020/10/29 19:47
 * @Created by HanYuHao
 */
@Component
public class ProfileUtil implements ApplicationContextAware {

    private static ApplicationContext context = null;


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }

    public static String getActiveProfile(){
        String[] profiles = context.getEnvironment().getActiveProfiles();
        if(profiles.length != 0){
            return profiles[0];
        }
        return "";
    }

    public static String getPort() {
        return context.getEnvironment().getProperty("local.server.port");
    }
}
