package club.web51.classroom_tool.Enums;

/**
 * @Classname MsgEnum
 * @Description TODO
 * @Date 2020/10/28 17:43
 * @Created by HanYuHao
 */
public enum MsgEnum {

    OK(0,"操作成功"),
    ERROR(-1,"出现错误"),
    ERROR_USER_LOGIN(-2,"用户登录失败"),
    ERROR_USER_LOGIN_EXIST(-3,"用户不存在，登录失败"),
    ;


    private final Integer code;
    private final String description;

    MsgEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }



    public Integer getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }


}
