package club.web51.classroom_tool.Enums;

/**
 * @Classname UserTypeEnum
 * @Description TODO
 * @Date 2020/10/29 23:09
 * @Created by HanYuHao
 */
public enum UserTypeEnum {
    STUDENT(1,"学生",1),
    TEACHER(2,"教师",2),
    ADMIN(3,"管理员",3)
    ;
    private Integer type;
    private String name;
    private Integer root;

    UserTypeEnum(Integer type, String name, Integer root) {
        this.type = type;
        this.name = name;
        this.root = root;
    }

    public Integer getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public Integer getRoot() {
        return root;
    }
}
