package club.web51.classroominteractiontoolsadmin.Config;


import com.fasterxml.classmate.TypeResolver;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.data.domain.Pageable;
import springfox.documentation.schema.AlternateTypeRule;
import springfox.documentation.schema.AlternateTypeRuleConvention;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static springfox.documentation.schema.AlternateTypeRules.newRule;

/**
 * @Classname ccc
 * @Description TODO
 * @Date 2020/11/21 22:17
 * @Created by wxt
 */

@Configuration
@SuppressWarnings("SpringJavaAutowiringInspection")
public class PageableParamConfig {
    @Bean
    public AlternateTypeRuleConvention pageableConvention(final TypeResolver resolver) {
        return new AlternateTypeRuleConvention() {
            @Override
            public int getOrder() {
                return Ordered.LOWEST_PRECEDENCE;
            }

            @Override
            public List<AlternateTypeRule> rules() {
                return newArrayList(newRule(resolver.resolve(Pageable.class), resolver.resolve(Page.class)));
            }
        };
    }

    @ApiModel
    @Data
    static class Page {
        @ApiModelProperty(value = "页码(注意从0开始计数)")
        private Integer number;

        @ApiModelProperty("每页数据数量")
        private Integer size;

//        @ApiModelProperty("数据偏移量")
//        private Long offset;

//        @ApiModelProperty("是否配置分页")
//        private Long paged;

        @ApiModelProperty("是否按属性排序,格式:属性(,asc|desc)")
        private List<String> sort;
    }
}
