package club.web51.classroominteractiontoolsadmin.Security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Classname RequireRole
 * @Description TODO
 * @Date 2020/11/19 23:58
 * @Created by HanYuHao
 */
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface RequireRole {
    boolean required() default true;
    String role() default "";
}
