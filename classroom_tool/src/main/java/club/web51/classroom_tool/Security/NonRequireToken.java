package club.web51.classroominteractiontoolsadmin.Security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Classname NonRequireToken
 * @Description TODO
 * @Date 2020/11/19 23:55
 * @Created by HanYuHao
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface NonRequireToken {
    boolean required() default true;
}
