package club.web51.classroominteractiontoolsadmin.Enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Classname SignTypeEnum
 * @Description TODO
 * @Created by yln
 */
@AllArgsConstructor
@Getter
public enum SignTypeEnum {
    SIGN_NORMAL(1,"密码签到"),
    SIGN_PHOTO(2,"拍照签到"),
    SIGN_LOCATION(3,"定位签到"),
    SIGN_HAND(4,"手势签到"),
    SIGN_QR(5,"二维码签到"),
    ;
    private Integer code;
    private String name;
}
