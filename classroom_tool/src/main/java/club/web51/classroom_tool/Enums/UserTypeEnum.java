package club.web51.classroominteractiontoolsadmin.Enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Classname UserTypeEnum
 * @Description TODO
 * @Date 2020/10/29 23:09
 * @Created by yln
 */
@AllArgsConstructor
@Getter
public enum UserTypeEnum {
    STUDENT(1,"学生",1),
    TEACHER(2,"教师",2),
    ADMIN(3,"管理员",3)
    ;
    private Integer type;
    private String name;
    private Integer root;

}
