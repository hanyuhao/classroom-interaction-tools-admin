package club.web51.classroominteractiontoolsadmin.Enums;

/**
 * @Classname MsgEnum
 * @Description TODO
 * @Created by yln
 */
public enum MsgEnum {

    //-17

    OK(0,"成功"),
    ERROR(-1,"出现错误"),
    ERROR_INVALID_OPREATION(-16,"非法操作"),
    ERROR_LOGIN_FAIL(-2,"登录失败"),
    ERROR_LOGINED(-14,"您已经登录，无需重复登录"),
    ERROR_USER_LOGIN_EXIST(-3,"用户不匹配，登录失败"),
    ERROR_USER_LOCKED(-5,"用户已经被锁定"),
    ERROR_USER_DATA_ERR(-8,"用户数据错误"),
    ERROR_USER_NAME_EXIST(-10,"用户名已经存在"),
    ERROR_USER_DATA_INVAILD(-13,"用户信息无效，请重新登录"),
    ERROR_USER_NOT_EXIST(-17,"用户不存在"),
    ERROR_USER_EXIST(-15,"用户已经存在"),
    ERROR_DATA_EMPTY(-4,"数据为空或不足"),
    ERROR_DATA_EXCEPT(-9,"数据异常"),
    ERROR_NO_ROOT(-6,"您没有权限,请先登录"),
    ERROR_NO_ALL_ROOT(-7,"您的权限不足"),
    ERROR_NO_ROOT_SIGN(-11,"签到时间已过，下次早点哦"),
    ERROR_ALREADY_SIGN(-12,"您已经签到过！"),
    ERROR_COURSE_NOT_EXIST(-18,"课程不存在"),
    ;


    private final Integer code;
    private final String description;

    MsgEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }


    public Integer getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }


}
