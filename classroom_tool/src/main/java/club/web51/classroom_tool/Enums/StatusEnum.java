package club.web51.classroominteractiontoolsadmin.Enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Classname StatusEnum
 * @Description TODO
 * @Date 2020/11/10 13:29
 * @Created by HanYuHao
 */
@Getter
@AllArgsConstructor
public enum StatusEnum {
    UNAVAILABLE(0,"不可用"),
    OK(1,"正常"),
    LOCK(2,"锁定"),
    HIDE(3,"隐藏(删除)"),
    STAR(4,"星标"),
    ;
    private final Integer status;
    private final String name;

}
