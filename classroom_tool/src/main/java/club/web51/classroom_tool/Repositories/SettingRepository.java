package club.web51.classroominteractiontoolsadmin.Repositories;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import club.web51.classroominteractiontoolsadmin.DO.MainDO.Setting;

/**
 * 服务类
 * @author yln
 * @since 1.0.0
 */
@Repository
public interface SettingRepository extends JpaRepository<Setting,Long>,JpaSpecificationExecutor<Setting>{

}