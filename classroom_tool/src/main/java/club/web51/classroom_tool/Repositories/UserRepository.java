package club.web51.classroominteractiontoolsadmin.Repositories;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import club.web51.classroominteractiontoolsadmin.DO.MainDO.User;

/**
 * 服务类
 * @author cxk
 * @since 1.0.0
 */
@Repository
public interface UserRepository extends JpaRepository<User,Long>,JpaSpecificationExecutor<User>{

}