package club.web51.classroominteractiontoolsadmin.Repositories;
import club.web51.classroominteractiontoolsadmin.DO.MainDO.Section;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;



/**
 * 服务类
 * @author cxk
 * @since 1.0.0
 */
@Repository
public interface SectionRepository extends JpaRepository<Section,Long>,JpaSpecificationExecutor<Section>{

}