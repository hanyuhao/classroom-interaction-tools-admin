package club.web51.classroominteractiontoolsadmin.Repositories;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import club.web51.classroominteractiontoolsadmin.DO.MainDO.Course;

/**
 * 服务类
 * @author wxt
 * @since 1.0.0
 */
@Repository
public interface CourseRepository extends JpaRepository<Course,Long>,JpaSpecificationExecutor<Course>{

}