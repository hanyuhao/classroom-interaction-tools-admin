package club.web51.classroominteractiontoolsadmin.Repositories;


import club.web51.classroominteractiontoolsadmin.DO.MainDO.Notice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @Classname NoticeRepository
 * @Description TODO
 * @Date 2020/11/22 0:47
 * @Created by wlh
 */
@Repository
public interface NoticeRepository extends JpaRepository<Notice,Long>, JpaSpecificationExecutor<Notice> {
}
