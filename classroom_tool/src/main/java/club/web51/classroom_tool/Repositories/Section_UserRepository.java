package club.web51.classroominteractiontoolsadmin.Repositories;
import club.web51.classroominteractiontoolsadmin.DO.RelationDO.Section_User;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;



/**
 * 服务类
 * @author cxk
 * @since 1.0.0
 */
@Repository
public interface Section_UserRepository extends JpaRepository<Section_User,Long>,JpaSpecificationExecutor<Section_User>{

}