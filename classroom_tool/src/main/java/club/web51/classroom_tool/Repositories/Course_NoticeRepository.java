package club.web51.classroominteractiontoolsadmin.Repositories;

import club.web51.classroominteractiontoolsadmin.DO.RelationDO.Course_Notice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @Classname Course_NoticeRepository
 * @Description TODO
 * @Date 2020/12/14 0:48
 * @Created by wxt
 */
@Repository
public interface Course_NoticeRepository extends JpaRepository<Course_Notice,Long>, JpaSpecificationExecutor<Course_Notice> {
}
