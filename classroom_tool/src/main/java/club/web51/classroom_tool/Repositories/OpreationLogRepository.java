package club.web51.classroominteractiontoolsadmin.Repositories;

import club.web51.classroominteractiontoolsadmin.DO.MainDO.Notice;
import club.web51.classroominteractiontoolsadmin.DO.SystemDO.OpreationLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @Classname OpreationLog
 * @Description TODO
 * @Created by cxk
 */



@Repository
public interface OpreationLogRepository extends JpaRepository<OpreationLog,Long>, JpaSpecificationExecutor<OpreationLog> {
}
