package club.web51.classroominteractiontoolsadmin.Services;

import club.web51.classroominteractiontoolsadmin.DO.MainDO.Course;
import club.web51.classroominteractiontoolsadmin.DO.MainDO.Notice;
import club.web51.classroominteractiontoolsadmin.DO.RelationDO.Course_Notice;
import club.web51.classroominteractiontoolsadmin.Enums.MsgEnum;
import club.web51.classroominteractiontoolsadmin.Enums.StatusEnum;
import club.web51.classroominteractiontoolsadmin.Exceptions.ErrDataException;
import club.web51.classroominteractiontoolsadmin.Repositories.CourseRepository;
import club.web51.classroominteractiontoolsadmin.Repositories.Course_NoticeRepository;
import club.web51.classroominteractiontoolsadmin.Repositories.NoticeRepository;
import club.web51.classroominteractiontoolsadmin.Utils.BeanPlusUtils;
import club.web51.classroominteractiontoolsadmin.Utils.SFWhere;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Classname NoticeService
 * @Description TODO 公告服务
 * @Date 2020/11/22 0:50
 * @Created by wlh
 */

@Service
//@CacheConfig(cacheNames = {"noticeCache"})
public class NoticeService {
    @Resource
    private NoticeRepository noticeRepository;
//    @Resource
//    private Course_NoticeRepository course_noticeRepository;
//    @Resource
//    private CourseRepository courseRepository;

    /**
     * 查询所有公告
     * @param notice 公告体
     * @param pageable 分页参数
     * @return 公告数据（分页）
     */
//   @Cacheable
    public Page<Notice> findAll(Notice notice, Pageable pageable){

        if(Objects.isNull(notice)){
            notice = Notice.builder().status(StatusEnum.OK.getStatus()).build();
        }

        if (notice.getIsSystem()){
            Notice sys = Notice.builder().status(StatusEnum.OK.getStatus()).isSystem(true).build();
            return noticeRepository.findAll(SFWhere.and(sys).build(),pageable);
        }
        notice.setStatus(StatusEnum.OK.getStatus());
        return noticeRepository.findAll(SFWhere.and(notice).build(),pageable);
    }

    /**
     * 获取课程公告
     * @param notice 公告
     * @param pageable 分页参数
     * @return 返回公告数据
     */
//    @Cacheable
    public Page<Notice> findByCourse(Notice notice, Pageable pageable){
        if(Objects.isNull(notice)){
            notice = Notice.builder().status(StatusEnum.OK.getStatus()).build();
        }
        notice.setStatus(StatusEnum.OK.getStatus());
        Page<Notice> page = noticeRepository.findAll(SFWhere.and(notice).build(),pageable);
        return page;
//        //构造查询实体初始化
//        notice.setStatus(StatusEnum.OK.getStatus());
//        Course course = Course.builder().cid(cid).status(StatusEnum.OK.getStatus()).build();
//        Course_Notice course_notice = Course_Notice.builder().cid(cid).build();
//        //cid非法
//        Optional<Course> course1 = courseRepository.findOne(SFWhere.and(course).build());
//        if(!course1.isPresent()){
//            throw new ErrDataException(MsgEnum.ERROR_COURSE_NOT_EXIST);
//        }
//        //获取Course_Notice关联表获取Notice列表
//        Page<Course_Notice> course_notices = course_noticeRepository.findAll(SFWhere.and(course_notice).build(),pageable);
//        Pageable pageable1 = course_notices.getPageable();
//        //构造list列表，做去重处理
//        List<Long> noticeids = course_notices.stream().map((e)->{
//            return e.getNid();
//        }).distinct().collect(Collectors.toList());
//        List<Notice> notices = noticeids.stream().map((e)->{
//            Notice notice1 = Notice.builder().nid(e).status(StatusEnum.OK.getStatus()).build();
//            if(noticeRepository.findOne(SFWhere.and(notice1).build()).isPresent())
//            return noticeRepository.getOne(e);
//            else return null;
//        }).filter(Objects::nonNull).collect(Collectors.toList());
//        //构造分页类
//        Page<Notice> noticepage = new PageImpl<Notice>(notices,pageable1,course_notices.getTotalElements());
//        //返回
//        return noticepage;
    }


//    @CachePut
    public void addOne(Notice notice){
        notice.setNid(null);
        noticeRepository.save(notice);
    }

    /**
     * 增加课程
     * @param notice 公告
     * @param cid 课程id
     */
//    @CachePut
    public void addOne(Notice notice,Long cid){

        notice.setNid(null);
        notice.setCid(cid);
        noticeRepository.save(notice);

        //Course_Notice course_notice = Course_Notice.builder().nid(notice.getNid()).cid(cid).build();
        //course_noticeRepository.save(course_notice);
    }


//    @CachePut
    public void updateOne(Notice notice){
        if(Objects.isNull(notice.getNid())){
            throw new ErrDataException(MsgEnum.ERROR_DATA_EXCEPT);
        }
        Notice notice1 = noticeRepository.getOne(notice.getNid());
//        //禁止修改类型
//        notice.setIsSystem(null);
        BeanPlusUtils.copyPropertiesIgnoreNull(notice,notice1);
        noticeRepository.save(notice1);
    }


//    @CacheEvict
    public void deleteOne(Long id){
        Notice notice = Notice.builder().nid(id).status(StatusEnum.OK.getStatus()).build();
        Optional<Notice> one = noticeRepository.findOne(SFWhere.and(notice).build());
        if(!one.isPresent()){
            throw new ErrDataException(MsgEnum.ERROR_DATA_EXCEPT);
        }
        Notice notice1 = one.get();
        notice1.setStatus(StatusEnum.HIDE.getStatus());
        noticeRepository.save(notice1);
    }
}
