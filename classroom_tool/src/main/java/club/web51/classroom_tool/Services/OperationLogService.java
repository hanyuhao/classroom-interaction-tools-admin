package club.web51.classroominteractiontoolsadmin.Services;

import club.web51.classroominteractiontoolsadmin.DO.SystemDO.OpreationLog;
import club.web51.classroominteractiontoolsadmin.Enums.StatusEnum;
import club.web51.classroominteractiontoolsadmin.Repositories.OpreationLogRepository;
import club.web51.classroominteractiontoolsadmin.Utils.SFWhere;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import springfox.documentation.service.Operation;

import javax.annotation.Resource;

/**
 * @Classname OperationLogService
 * @Description TODO
 * @Created by cxk
 */
@Service
public class OperationLogService {
    @Resource
    private OpreationLogRepository opreationLogRepository;
    /**
     * 获取日志列表
     * @param opreationLog 日志参数
     * @param pageable 分页信息
     * @return 分页后的日志信息
     */
    
    public Page<OpreationLog> getLog(OpreationLog opreationLog,Pageable pageable){
        opreationLog.setStatus(StatusEnum.OK.getStatus());
        return opreationLogRepository.findAll(SFWhere.and(opreationLog).build(),pageable);
    }
}
