package club.web51.classroominteractiontoolsadmin.Services;

import club.web51.classroominteractiontoolsadmin.DO.MainDO.Course;
import club.web51.classroominteractiontoolsadmin.DO.SystemDO.Msg;
import club.web51.classroominteractiontoolsadmin.Enums.StatusEnum;
import club.web51.classroominteractiontoolsadmin.Utils.BeanPlusUtils;
import club.web51.classroominteractiontoolsadmin.Utils.MsgUtil;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired ; 
import club.web51.classroominteractiontoolsadmin.Repositories.UserRepository;
import club.web51.classroominteractiontoolsadmin.Utils.SFWhere;
import club.web51.classroominteractiontoolsadmin.DO.MainDO.User;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * 服务类
 * @author yln
 * @since 1.0.0
 */
@Service
public class UserService {
    @Resource
    UserRepository userRepository;
    /**
     * 根据User的字段自动生成条件,字段值为null不生成条件
     * 如果是数值型的字段,前端不传入值,默认是0,例如ID的类型是Long,如果不传值,默认是0
     * 可以自己设置下SFWhere.and(user).equal(实体.getId()>0,"id",实体.getId()).build()
     * @param user     实体对象
     * @param pageable 分页对象
     * @return 返回分页\状态码
     */
    public Page<User> search(User user, Pageable pageable) {
//        System.out.println(Objects.isNull(userRepository));
        System.out.println(user);
        Page<User> all = userRepository.
                findAll(SFWhere.and(user)
                .equal(true,"status", StatusEnum.OK.getStatus())
                //.equal(user.getId() > 0, "id", user.getId())
                //.in(true, "userName", longs)
                //.equal(!字段值的判断.equals("") && 字段值的判断 != null, "字段名称", 字段值)
                .like((Objects.nonNull(user.getName()) && !"".equals(user.getName())), "name", "%" + user.getName() + "%")
                //....
                .build(), pageable);
        return all;
    }

    public Msg create(User user){
        if(Objects.isNull(user.getRole()))
            user.setRole("student");
        if(Objects.isNull(user.getPassword()))
            user.setPassword("123456");
        userRepository.save(user);
        return MsgUtil.success();
    }

    public Msg update(User user){
        User user1 = userRepository.getOne(user.getUid());
        BeanPlusUtils.copyPropertiesIgnoreNull(user,user1);
        userRepository.save(user1);
        return MsgUtil.success();
    }

    public Msg delete(Long id){
        User user = userRepository.getOne(id);
        user.setStatus(StatusEnum.HIDE.getStatus());
        userRepository.save(user);
        return MsgUtil.success();
    }

    public Msg login(User user){
        User user1 = User.builder().uid(user.getUid()).password(user.getPassword()).build();
        User user2 = userRepository.getOne(user1.getUid());
        if(user2.getPassword().equals(user1.getPassword())){
            return MsgUtil.success("登录成功");
        }else{
            return MsgUtil.fail("登录失败");
        }
    }
}
