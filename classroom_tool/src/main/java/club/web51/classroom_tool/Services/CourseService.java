package club.web51.classroominteractiontoolsadmin.Services;

import club.web51.classroominteractiontoolsadmin.DO.SystemDO.Msg;
import club.web51.classroominteractiontoolsadmin.Enums.StatusEnum;
import club.web51.classroominteractiontoolsadmin.Utils.BeanPlusUtils;
import club.web51.classroominteractiontoolsadmin.Utils.MsgUtil;
import org.hibernate.annotations.Cache;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired ; 
import club.web51.classroominteractiontoolsadmin.Repositories.CourseRepository;
import club.web51.classroominteractiontoolsadmin.Utils.SFWhere;
import club.web51.classroominteractiontoolsadmin.DO.MainDO.Course;

import javax.annotation.Resource;

/**
 * 服务类
 * @author wxt
 * @since 1.0.0
 */
@Service
@CacheConfig(cacheNames = {"CourseCache"})
public class CourseService {
    @Resource
    CourseRepository courseRepository;
    /**
     * 根据Course的字段自动生成条件,字段值为null不生成条件
     * 如果是数值型的字段,前端不传入值,默认是0,例如ID的类型是Long,如果不传值,默认是0
     * 可以自己设置下SFWhere.and(course).equal(实体.getId()>0,"id",实体.getId()).build()
     * @param course   实体对象
     * @param pageable 分页对象
     * @return 返回分页\状态码
     */
    @Cacheable(value = "course",key = "#course.cid")
    public Msg search(Course course, Pageable pageable) {
        Page<Course> all = courseRepository.findAll(SFWhere.and(course)
                .equal(true,"status", StatusEnum.OK.getStatus())
                //.in(true, "userName", longs)
                //.equal(!字段值的判断.equals("") && 字段值的判断 != null, "字段名称", 字段值)
                //.like(字段值的判断 != null, "字段名称", "%" + 字段值 + "%")
                //....
                .build(), pageable);
        return MsgUtil.success("数据获取成功",all);
    }


    @CachePut(value = "course",key = "#course.cid")
    public Msg update(Course course){
        Course course1 = courseRepository.getOne(course.getCid());
        BeanPlusUtils.copyPropertiesIgnoreNull(course,course1);
        courseRepository.save(course1);
        return MsgUtil.success();
    }

    @CachePut(value = "course",key = "#course.cid")
    public Msg create(Course course){
        course.setCid(null);
        courseRepository.save(course);
        return MsgUtil.success();
    }

    @CacheEvict(value = "course")
    public Msg delete(Long id){
        Course course = Course.builder().status(StatusEnum.OK.getStatus()).cid(id).build();
        Course course1 = courseRepository.findOne(SFWhere.and(course).build()).orElse(course);
        course1.setStatus(StatusEnum.HIDE.getStatus());
        courseRepository.save(course1);
        return MsgUtil.success();
    }

}
