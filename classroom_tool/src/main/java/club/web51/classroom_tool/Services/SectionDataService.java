package club.web51.classroominteractiontoolsadmin.Services;

import club.web51.classroominteractiontoolsadmin.DO.MainDO.Section;
import club.web51.classroominteractiontoolsadmin.DO.MainDO.User;
import club.web51.classroominteractiontoolsadmin.DO.RelationDO.Section_User;
import club.web51.classroominteractiontoolsadmin.Enums.MsgEnum;
import club.web51.classroominteractiontoolsadmin.Enums.StatusEnum;
import club.web51.classroominteractiontoolsadmin.Exceptions.ErrDataException;
import club.web51.classroominteractiontoolsadmin.Repositories.Section_UserRepository;
import club.web51.classroominteractiontoolsadmin.Repositories.UserRepository;
import club.web51.classroominteractiontoolsadmin.Utils.SFWhere;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Classname SectionDataService
 * @Description TODO
 * @Created by cxk
 */
@Service
public class SectionDataService {
    @Resource
    Section_UserRepository sectionUserRepository;
    @Resource
    UserRepository userRepository;

    /**
     * 查找群组人员
     * @param seid 群组ID
     * @param cid 课程ID
     * @param pageable 分页信息
     * @return 群组的用户实体信息
     */
    public Page<User> getUserBySection(Long seid,Long cid, Pageable pageable){
        //TODO 判断群组状态
        //构造搜索实体
        Section_User section_user = Section_User.builder().seid(seid).cid(cid).build();
        //获取所有用户ID
        Page<Section_User> section_users = sectionUserRepository.findAll(SFWhere.and(section_user).build(),pageable);
        //获取所有用户的分页信息
        Pageable pageable1 = section_users.getPageable();
        //构造用户列表
        List<User> users = section_users.get().map((su)->{
            //构造搜索实体
            User u = User.builder().uid(su.getUid()).status(StatusEnum.OK.getStatus()).build();
            Optional<User> userOptional = userRepository.findOne(SFWhere.and(u).build());
            return userOptional.orElse(null);
        }).collect(Collectors.toList());
        //构造page类
        Page<User> userPage = new PageImpl<User>(users,pageable,section_users.getTotalElements());
        //返回
        return userPage;
    }

    /**
     * 群组添加单个人员
     * @param seid 群组id
     * @param uid 用户id
     */
    public void addOne(Long seid,Long uid){
        User user = User.builder().status(StatusEnum.OK.getStatus()).uid(uid).build();
        if(userRepository.count(SFWhere.and(user).build())!=1){
            throw new ErrDataException(MsgEnum.ERROR_USER_NOT_EXIST);
        }
        Section_User section_user = Section_User.builder().seid(seid).uid(uid).build();
        if(sectionUserRepository.count(SFWhere.and(section_user).build()) > 0){
            throw new ErrDataException(MsgEnum.ERROR_USER_EXIST);
        }
        sectionUserRepository.save(section_user);
    }
}
