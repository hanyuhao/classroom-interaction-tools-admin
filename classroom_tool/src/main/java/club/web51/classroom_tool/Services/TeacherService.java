package club.web51.classroominteractiontoolsadmin.Services;

import club.web51.classroominteractiontoolsadmin.DO.MainDO.Course;
import club.web51.classroominteractiontoolsadmin.Repositories.CourseRepository;
import club.web51.classroominteractiontoolsadmin.Utils.SFWhere;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Classname TeacherService
 * @Description TODO
 * @Created by cxk
 */
@Service
public class TeacherService {
    @Resource
    CourseRepository courseRepository;
    
    /**
     * 查找教师课程
     * @param uid 教师uid
     * @return 课程列表
     */

    public Page<Course> findCourseByUid(Long uid, Pageable pageable){
        Course course = Course.builder().uid(uid).build();
        Page<Course> courses = courseRepository.findAll(SFWhere.and(course).build(),pageable);
        return courses;
    }
}
