package club.web51.classroominteractiontoolsadmin.Services;

import club.web51.classroominteractiontoolsadmin.DO.MainDO.Course;
import club.web51.classroominteractiontoolsadmin.DO.SystemDO.Msg;
import club.web51.classroominteractiontoolsadmin.Enums.StatusEnum;
import club.web51.classroominteractiontoolsadmin.Utils.BeanPlusUtils;
import club.web51.classroominteractiontoolsadmin.Utils.MsgUtil;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired ; 
import club.web51.classroominteractiontoolsadmin.Repositories.SignRepository;
import club.web51.classroominteractiontoolsadmin.Utils.SFWhere;
import club.web51.classroominteractiontoolsadmin.DO.MainDO.Sign;

/**
 * 服务类
 * @author cxk
 * @since 1.0.0
 */
@Service
public class SignService {
    @Autowired
    SignRepository signRepository;
    /**
     * 根据Sign的字段自动生成条件,字段值为null不生成条件
     * 如果是数值型的字段,前端不传入值,默认是0,例如ID的类型是Long,如果不传值,默认是0
     * 可以自己设置下SFWhere.and(sign).equal(实体.getId()>0,"id",实体.getId()).build()
     * @param sign     实体对象
     * @param pageable 分页对象
     * @return 返回分页\状态码
     */
    public Msg search(Sign sign, Pageable pageable) {
        Page<Sign> all = signRepository.findAll(SFWhere.and(sign)
                .equal(true,"status", StatusEnum.OK.getStatus())
                //.equal(user.getId() > 0, "id", user.getId())
                //.in(true, "userName", longs)
                //.equal(!字段值的判断.equals("") && 字段值的判断 != null, "字段名称", 字段值)
                //.like(字段值的判断 != null, "字段名称", "%" + 字段值 + "%")
                //....
                .build(), pageable);
        return MsgUtil.success("数据获取成功",all);
    }
    public Msg create(Sign sign){
        //sign.setCid(null);
        return MsgUtil.success("签到创建成功",signRepository.save(sign));
    }
    public Msg update(Sign sign){
        Sign sign1 = signRepository.getOne(sign.getSiid());
        BeanPlusUtils.copyPropertiesIgnoreNull(sign,sign1);
        signRepository.save(sign1);
        return MsgUtil.success();
    }
    public Msg delete(String id){
        Sign sign = signRepository.getOne(id);
        sign.setStatus(StatusEnum.HIDE.getStatus());
        return MsgUtil.success();
    }
}
