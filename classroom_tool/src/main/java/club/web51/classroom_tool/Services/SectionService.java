package club.web51.classroominteractiontoolsadmin.Services;

import club.web51.classroominteractiontoolsadmin.DO.MainDO.Course;
import club.web51.classroominteractiontoolsadmin.DO.MainDO.Section;
import club.web51.classroominteractiontoolsadmin.DO.SystemDO.Msg;
import club.web51.classroominteractiontoolsadmin.Enums.StatusEnum;
import club.web51.classroominteractiontoolsadmin.Repositories.SectionRepository;
import club.web51.classroominteractiontoolsadmin.Utils.BeanPlusUtils;
import club.web51.classroominteractiontoolsadmin.Utils.MsgUtil;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired ;
import club.web51.classroominteractiontoolsadmin.Utils.SFWhere;


/**
 * 服务类
 * @author cxk
 * @since 1.0.0
 */
@Service
public class SectionService {
    @Autowired
    SectionRepository sectionRepository;
    /**
     * 根据Group的字段自动生成条件,字段值为null不生成条件
     * 如果是数值型的字段,前端不传入值,默认是0,例如ID的类型是Long,如果不传值,默认是0
     * 可以自己设置下SFWhere.and(group).equal(实体.getId()>0,"id",实体.getId()).build()
     * @param section     实体对象
     * @param pageable 分页对象
     * @return 返回分页\状态码
     */
    public Msg search(Section section, Pageable pageable) {
        Page<Section> all = sectionRepository.findAll(SFWhere.and(section)
                .equal(true,"status", StatusEnum.OK.getStatus())
                .build(), pageable);
        return MsgUtil.success("数据获取成功",all);
    }

    public Msg create(Section section){
        section.setSeid(null);
        sectionRepository.save(section);
        return MsgUtil.success();
    }

    public Msg update(Section section){
        Section section1 = sectionRepository.getOne(section.getSeid());
        BeanPlusUtils.copyPropertiesIgnoreNull(section,section1);
        sectionRepository.save(section1);
        return MsgUtil.success();
    }

    public Msg delete(Long id){
        Section section = sectionRepository.getOne(id);
        section.setStatus(StatusEnum.HIDE.getStatus());
        return MsgUtil.success();
    }
}
