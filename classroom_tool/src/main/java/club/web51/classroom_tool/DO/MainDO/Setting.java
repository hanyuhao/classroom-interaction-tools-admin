package club.web51.classroominteractiontoolsadmin.DO.MainDO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @Classname Setting
 * @Description TODO 系统设置
 * @Created by yln
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@ApiModel(value = "系统设置")
public class Setting {
    @Id
    @GeneratedValue
    private Long setid;
    /**
     * 名称
     */
    @ApiModelProperty(value = "系统设置名")
    @Column(nullable = false,unique = true)
    private String name;
    /**
     * 值
     */
    @Column
    @ApiModelProperty(value = "系统设置值")
    private String value;
}
