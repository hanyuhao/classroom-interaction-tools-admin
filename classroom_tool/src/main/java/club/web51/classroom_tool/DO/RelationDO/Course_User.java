package club.web51.classroominteractiontoolsadmin.DO.RelationDO;

import club.web51.classroominteractiontoolsadmin.Enums.StatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiParam;

import javax.persistence.*;
import java.util.Objects;

/**
 * @Classname Course_User
 * @Description TODO 课程-学生关联
 * @Date 2020/12/14 1:03
 * @Created by wxt
 */
@Entity
public class Course_User {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    @ApiParam(value = "课程ID")
    private Long cid;
    @ApiParam(value = "用户ID")
    @Column
    private Long uid;
    @Column
    @JsonIgnore
    private Integer status;

    @PrePersist
    void setDefaultStatus(){
        if(Objects.isNull(status)){
            status = StatusEnum.OK.getStatus();
        }
    }
}
