package club.web51.classroominteractiontoolsadmin.DO.MainDO;

import club.web51.classroominteractiontoolsadmin.Enums.StatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.*;
import org.hibernate.validator.constraints.Range;
import springfox.documentation.annotations.ApiIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Objects;

/**
 * @Classname Course
 * @Description TODO 课程
 * @Date 2020/12/13 20:03
 * @Created by wxt
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@ApiModel(value = "课程类",description = "跟课程有关的类")
public class Course {
    @Id
    @GeneratedValue
    @ApiModelProperty(value = "课程ID")
    private Long cid;
    /**
     * 课程名
     */
    @Column
    @ApiModelProperty(value = "课程名")
//    @NotEmpty(message = "课程名不能为空")
    private String name;
    /**
     * 学分
     */
    @Column
    @ApiModelProperty(value = "课程学分")
//    @NotNull(message = "学分必须填写")
    private Float point;
    /**
     * 容量
     */
    @Column
//    @NotNull(message = "容量必须填写")
//    @Range(min = 1,max = 9999,message = "容量应该在1-9999之间")
    @ApiModelProperty(value = "课程容量")
    private Integer capacity;
    /**
     * 教师ID
     */
//    @Min(message = "请输入合法的教师ID", value = 1L)
    @ApiModelProperty(value = "课程教师ID")
    private Long uid;
    /**
     * 描述
     */
    @Column
    @ApiModelProperty(value = "课程描述")
    private String description;
    @Column
    @JsonIgnore
    @ApiModelProperty(value = "课程状态",hidden = true)
    private Integer status;

    @PrePersist
    public void preset(){
        if(Objects.isNull(status)){
            status = StatusEnum.OK.getStatus();
        }
    }
}
