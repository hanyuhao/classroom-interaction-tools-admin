package club.web51.classroominteractiontoolsadmin.DO.MainDO;

import club.web51.classroominteractiontoolsadmin.Enums.StatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Proxy;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Objects;
import java.util.Random;

/**
 * @Classname User
 * @Description TODO 用户（学生/老师/管理员）
 * @Created by yln
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@ApiModel(value = "用户实体")
@Proxy(lazy = false)
public class User {
    @Id
    @GeneratedValue
    @ApiModelProperty(value = "用户ID")
    private Long uid;
    @Column(unique = true)
    @ApiModelProperty(value = "用户学号/工号")
    private Long number;
//    @NotNull
    @ApiModelProperty(value = "用户状态",hidden = true)
    private Integer status;
//    @NotEmpty(message = "姓名不能为空")
    @ApiModelProperty(value = "用户姓名")
    @Column
    private String name;
//    @NotNull(message = "手机号必须填写")
    @ApiModelProperty(value = "用户手机号")
    @Column(length = 11)
    private Long telephone;
    @Column
//    @Pattern(regexp = "[男女]",message = "性别只能是 男 或 女 ！")
    @ApiModelProperty(value = "用户性别")
    private String sex;
    @Column(unique = true)
//    @NotEmpty(message = "用户名不能为空")
    @ApiModelProperty(value = "用户名")
    private String username;
    @JsonIgnore
    @Column
//    @Size(max = 16,min = 8,message = "密码长度在8-16位之间！")
    @ApiModelProperty(value = "用户密码")
    private String password;
    @Column
    @ApiModelProperty(value = "用户位置")
    private String location;
    @JsonIgnore
    @Column
    @ApiModelProperty(value = "用户盐，前端无视")
    private String salt;
    @CreationTimestamp
    @ApiModelProperty(value = "用户创建时间，前端无视")
    private Date creatTime;
    @UpdateTimestamp
    @ApiModelProperty(value = "用户修改时间，前端无视")
    private Date updateTime;
    /**
     * 角色信息
     */
    @Column
    @ApiModelProperty(value = "用户角色，前端勿改")
    private String role;
    /**
     * 为Salt字段预生成一个随机的salt
     * 初始化正常状态
     */
    @PrePersist
    public void preSetSalt(){
        if(Objects.isNull(status)){
            status = StatusEnum.OK.getStatus();
        }
        if(Objects.isNull(password)){
            password = "123456";
        }

        if(Objects.nonNull(getSalt())){
            return;
        }
        /**
         * 指定随机字段长度
         */
        int length = 6;
        String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for(int i=0;i<length;i++){
            int number=random.nextInt(62);
            sb.append(str.charAt(number));
        }
        setSalt(sb.toString());
    }

}
