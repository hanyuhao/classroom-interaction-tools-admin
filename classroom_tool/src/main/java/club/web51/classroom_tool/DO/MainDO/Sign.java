package club.web51.classroominteractiontoolsadmin.DO.MainDO;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * @Classname Sign
 * @Description TODO 签到条目
 * @Created by cxk
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Sign {
    @Id
    @GenericGenerator(name = "jpa-uuid", strategy = "uuid")
    @GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
    private String siid;
    /**
     * 签到名
     */
    @Column
    private String name;
    /**
     * 签到条目归属用户
     */
    @Column
    private Long uid;
    /**
     * 签到条目所属课程
     */
    private Long cid;
    /**
     * 签到条目状态
     */
    @Column
    private Integer status;
    /**
     * 签到种类
     */
    private Integer kind;
    /**
     * 截止日期
     */
    @Column
    private Date deadline;
}
