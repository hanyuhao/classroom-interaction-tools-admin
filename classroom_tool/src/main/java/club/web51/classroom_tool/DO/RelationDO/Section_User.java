package club.web51.classroominteractiontoolsadmin.DO.RelationDO;

import club.web51.classroominteractiontoolsadmin.Enums.StatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiParam;
import lombok.*;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.Objects;

/**
 * @Classname Group_User
 * @Description TODO 学生分组
 * @Created by yln
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Proxy(lazy = false)
public class Section_User {
    @GeneratedValue
    @Id
    private Long id;
    /**
     * 课程ID
     */
    @Column
    @ApiParam(value = "课程ID")
    private Long cid;
    /**
     * 群组ID
     */
    @Column
    @ApiParam(value = "群组ID")
    private Long seid;
    /**
     * 用户ID
     */
    @ApiParam(value = "用户ID")
    private Long uid;
    @Column
    @JsonIgnore
    private Integer status;

    @PrePersist
    void setDefaultStatus(){
        if(Objects.isNull(status)){
            status = StatusEnum.OK.getStatus();
        }
    }
}
