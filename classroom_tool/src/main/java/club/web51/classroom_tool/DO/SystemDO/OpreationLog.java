package club.web51.classroominteractiontoolsadmin.DO.SystemDO;

import club.web51.classroominteractiontoolsadmin.Enums.StatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

/**
 * @Classname OpreationLog
 * @Description TODO
 * @Created by cxk
 */
@ApiModel(value = "操作日志类",description = "各种对系统的操作的日志记录")
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OpreationLog {
    @Id
    @GeneratedValue
    @ApiModelProperty(value = "日志ID")
    private Long opid;
    @Column
    @ApiModelProperty(value = "操作类型 1 读 2 写")
    private Integer type;
    @Column
    @ApiModelProperty(value = "日志描述")
    private String description;
    @Column
    @ApiModelProperty(value = "操作用户ID")
    private Long uid;
    @CreationTimestamp
    @ApiModelProperty(value = "操作时间")
    private Date time;
    @Column
    @JsonIgnore
    @ApiModelProperty(value = "状态",hidden = true)
    private Integer status;

    @PrePersist
    void checkDeafult(){
        if(Objects.isNull(status)){
            setStatus(StatusEnum.OK.getStatus());
        }
    }
}
