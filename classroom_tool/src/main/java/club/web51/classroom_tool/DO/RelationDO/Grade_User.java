package club.web51.classroominteractiontoolsadmin.DO.RelationDO;

import club.web51.classroominteractiontoolsadmin.Enums.StatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiParam;

import javax.persistence.*;
import java.util.Objects;

/**
 * @Classname Grade_User
 * @Description TODO 成绩-学生类
 * @Created by yln
 */
@Entity
public class Grade_User {
    @Id
    @GeneratedValue
    private Long id;
    /**
     * 学生ID
     */
    @Column
    @ApiParam(value = "学生ID")
    private Long uid;
    /**
     * 课程ID
     */
    @Column
    @ApiParam(value = "课程ID")
    private Long cid;
    /**
     * 成绩
     */
    @Column
    @ApiParam(value = "课程成绩")
    private Float grade;

    @Column
    @JsonIgnore
    private Integer status;

    @PrePersist
    void setDefaultStatus(){
        if(Objects.isNull(status)){
            status = StatusEnum.OK.getStatus();
        }
    }
}
