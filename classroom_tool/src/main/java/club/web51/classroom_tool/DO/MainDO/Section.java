package club.web51.classroominteractiontoolsadmin.DO.MainDO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @Classname Section
 * @Description TODO 群组/班级
 * @Created by cxk
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Section {
    @Id
    @GeneratedValue
    private Long seid;
    /**
     * 群组名
     */
    @Column
    private String name;
    /**
     * 所属用户ID
     */
    @Column
    private Long uid;
    /**
     * 状态
     */
    @Column
    @JsonIgnore
    private Integer status;
}
