package club.web51.classroominteractiontoolsadmin.DO.RelationDO;

import club.web51.classroominteractiontoolsadmin.Enums.StatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Objects;

/**
 * @Classname Sign_User
 * @Description TODO 签到数据
 * @Created by yln
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@ApiModel(value = "签到数据")
public class Sign_User {
    @Id
    @GenericGenerator(name = "jpa-uuid", strategy = "uuid")
    @GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
    private String id;
    /**
     * 用户ID
     */
    @Column
    @ApiModelProperty(value = "用户ID")
    private Long uid;
    /**
     * 课程ID
     */
    @Column
    @ApiModelProperty(value = "课程ID")
    private Long cid;
    /**
     * 签到条目ID
     */
    @Column
    @ApiModelProperty(value = "签到条目ID")
    private String siid;
    /**
     * 签到类型
     */
    @Column
    @ApiModelProperty(value = "签到类型")
    private Integer sign_type;
    /**
     * 签到数据
     */
    @Column
    @ApiModelProperty(value = "签到数据")
    private String data;
    @Column
    @ApiModelProperty(value = "签到步骤",notes = "0 未签到 1已签到 2 其他")
    private Integer step;
    /**
     * 签到状态
     */
    @Column
    @JsonIgnore
    @ApiModelProperty(value = "数据状态",hidden = true)
    private Integer status;

    @Column
    @ApiModelProperty(value = "用户姓名")
    private String user_name_cache;

    @Column
    @ApiModelProperty(value = "用户学号")
    private Long user_number_cache;

    @Column
    @ApiModelProperty(value = "签到名称")
    private String sign_name_cache;

    @PrePersist
    void setDefaultStatus(){
        if(Objects.isNull(status)){
            status = StatusEnum.OK.getStatus();
        }
        if(Objects.isNull(step)){
            step = 0;
        }
    }

}
