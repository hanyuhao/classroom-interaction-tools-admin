package club.web51.classroominteractiontoolsadmin.DO.RelationDO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @Classname Course_Notice
 * @Description TODO 课程-公告关联
 * @Date 2020/11/22 0:43
 * @Created by wlh
 */
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Course_Notice {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private Long cid;
    @Column(nullable = false)
    private Long nid;
}
