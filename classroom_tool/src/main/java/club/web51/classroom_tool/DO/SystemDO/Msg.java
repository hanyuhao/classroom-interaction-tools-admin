package club.web51.classroominteractiontoolsadmin.DO.SystemDO;

import lombok.*;

/**
 * @Classname Msg
 * @Description TODO 通用消息返回体
 * @Created by yln
 */
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
@Builder
public class Msg {
    private Integer code;
    private String description;
    private Object data;
}
