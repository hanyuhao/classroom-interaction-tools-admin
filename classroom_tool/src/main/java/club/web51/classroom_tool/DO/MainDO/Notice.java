package club.web51.classroominteractiontoolsadmin.DO.MainDO;

import club.web51.classroominteractiontoolsadmin.Enums.StatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

/**
 * @Classname Course_Notice
 * @Description TODO
 * @Date 2020/11/22 0:29
 * @Created by wlh
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "公告条目类")
public class Notice {
    @Id
    @GeneratedValue
    @ApiModelProperty(value = "公告ID")
    private Long nid;
    @Column
    @ApiModelProperty(value = "公告标题")
    private String title;
    @Lob
    @ApiModelProperty(value = "公告正文，存储带html的文本内容")
    @Column
    private String context;
    @Column
    @ApiModelProperty(value = "发布者ID")
    private Long uid;
    @Column
    @ApiModelProperty(value = "作者名称（可以转载等,建议前端默认采用用户名，可以更改为自定义名称）")
    private String author;
    @Column
    @CreationTimestamp
    @ApiModelProperty(value = "创建时间(后端自动生成)")
    private Date createTime;
    @Column
    @UpdateTimestamp
    @ApiModelProperty(value = "更新时间(后端自动生成)")
    private Date updateTime;
    @Column
    @ApiModelProperty(value = "是否是系统消息,真 系统 假 课程")
    private Boolean isSystem;
    @ApiModelProperty(value = "课程ID")
    @Column
    private Long cid;
    @Column
    @JsonIgnore
    @ApiModelProperty(value = "状态",hidden = true)
    private Integer status;

    @PrePersist
    public void preset(){
        if(Objects.isNull(status)){
            status = StatusEnum.OK.getStatus();
        }
        if(Objects.isNull(isSystem)){
            isSystem = false;
        }
    }
}
