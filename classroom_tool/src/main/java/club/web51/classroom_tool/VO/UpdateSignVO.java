package club.web51.classroominteractiontoolsadmin.VO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Classname UpdateSignDTO
 * @Description 用户完成签到的数据转换模型
 * @Date 2020/12/21 17:03
 * @Created by HanYuHao
 */
@ApiModel(value = "用户完成签到模型")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UpdateSignVO {
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID")
    private Long uid;

    @ApiModelProperty(value = "用户学号")
    private Long number;
    /**
     * 签到条目ID
     */
    @ApiModelProperty(value = "签到条目ID")
    private String siid;
    /**
     * 提交数据
     */
    @ApiModelProperty(value = "签到提交数据")
    private String data;
}
