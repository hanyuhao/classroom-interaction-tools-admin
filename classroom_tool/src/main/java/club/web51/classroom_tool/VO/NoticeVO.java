package club.web51.classroominteractiontoolsadmin.VO;

import club.web51.classroominteractiontoolsadmin.DO.MainDO.Notice;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Classname NoticeVO
 * @Description TODO
 * @Date 2020/11/22 1:34
 * @Created by wlh
 */
@ApiModel(value = "创建选项")
@Data
public class NoticeVO {
    @ApiModelProperty(value = "公告")
    private Notice notice;
    @ApiModelProperty(value = "是否绑定课程，默认不绑定")
    private Boolean bindCourse;
    @ApiModelProperty(value = "课程ID")
    private Long cid;
}
