package club.web51.classroominteractiontoolsadmin.VO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * @Classname UserVO
 * @Description TODO
 * @Date 2020/11/14 17:31
 * @Created by HanYuHao
 */

@Data
@Builder
@ApiModel(value = "用户VO")
public class UserVO {
    @ApiModelProperty(value = "用户id")
    private Long uid;
    @ApiModelProperty(value = "用户学工号")
    private Long number;
    @ApiModelProperty(value = "密码")
    private String password;
    @ApiModelProperty(value = "令牌")
    private String token;
}
