package club.web51.classroominteractiontoolsadmin.VO;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @Classname TokenVO
 * @Description TODO
 * @Date 2020/11/20 1:11
 * @Created by HanYuHao
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiIgnore
public class TokenVO {
    @ApiModelProperty(value = "用户Token，前端无视",hidden = true)
    private String token;
    @ApiModelProperty(value = "用户uid，前端无视",hidden = true)
    private Long uid;
}
