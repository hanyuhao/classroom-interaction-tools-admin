package club.web51.classroominteractiontoolsadmin.VO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Classname SectionAddSingleVO
 * @Description TODO
 * @Date 2020/11/22 22:18
 * @Created by HanYuHao
 */
@Data
@ApiModel(value = "群组单个添加")
public class SectionAddSingleVO {
    @ApiModelProperty(value = "群组ID")
    private Long seid;
    @ApiModelProperty(value = "用户ID")
    private Long uid;
}
