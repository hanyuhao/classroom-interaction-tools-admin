package club.web51.classroominteractiontoolsadmin.Exceptions;


import club.web51.classroominteractiontoolsadmin.Enums.MsgEnum;

/**
 * @Classname NoDataException
 * @Description TODO
 * @Created by cxk
 */

public class ErrDataException extends RuntimeException{
    private final Integer code;
    public ErrDataException() {
        super(MsgEnum.ERROR.getDescription());
        this.code = MsgEnum.ERROR.getCode();
    }
    public ErrDataException(String message) {
        super(message);
        this.code = MsgEnum.ERROR.getCode();
    }

    public ErrDataException(MsgEnum msgEnum) {
        super(msgEnum.getDescription());
        this.code = msgEnum.getCode();
    }


    public Integer getCode() {
        return code;
    }
}
