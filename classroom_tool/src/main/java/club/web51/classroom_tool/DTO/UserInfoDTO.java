package club.web51.classroominteractiontoolsadmin.DTO;

import club.web51.classroominteractiontoolsadmin.DO.MainDO.User;
import com.google.common.collect.Lists;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @Classname UserInfoDTO
 * @Description TODO
 * @Created by yln
 */
@Data
@Getter
public class UserInfoDTO {

    private List<String> roles;
    private String introduction;
    private String avatar;
    private String name;
    private Long uid;
    private String sex;
    private Long telephone;

    public UserInfoDTO(User user) {

        this.roles = new ArrayList<String>();
        if(Objects.nonNull(user.getRole()) && user.getRole().split(" ").length > 0)
        roles.addAll(Arrays.asList(user.getRole().split(" ")));
        this.introduction = "This is introduction.";
        this.avatar = "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif";
        this.name = user.getName();
        this.uid = user.getUid();
        this.sex = user.getSex();
        this.telephone = user.getTelephone();
    }
}
