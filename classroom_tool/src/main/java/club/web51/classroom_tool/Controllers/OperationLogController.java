package club.web51.classroominteractiontoolsadmin.Controllers;

import club.web51.classroominteractiontoolsadmin.DO.SystemDO.OpreationLog;
import club.web51.classroominteractiontoolsadmin.Services.OperationLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.service.Operation;

import javax.annotation.Resource;

/**
 * @Classname OpreationLogController
 * @Description TODO
 * @Created by cxk
 */
@Api(tags = "操作日志类")
@RestController
@RequestMapping("log")
public class OperationLogController {
    @Resource
    private OperationLogService op;

    @ApiOperation(value = "获取日志",notes = "用于动态获取日志")
    @GetMapping("")
    public Page<OpreationLog> getLog(OpreationLog opreationLog,Pageable pageable){
        return op.getLog(opreationLog,pageable);
    }
}
