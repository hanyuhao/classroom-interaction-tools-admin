package club.web51.classroominteractiontoolsadmin.Controllers;

import club.web51.classroominteractiontoolsadmin.DO.SystemDO.Msg;
import club.web51.classroominteractiontoolsadmin.Services.TeacherService;
import club.web51.classroominteractiontoolsadmin.Utils.MsgUtil;
import club.web51.classroominteractiontoolsadmin.VO.TokenVO;
import club.web51.classroominteractiontoolsadmin.Security.RequireToken;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Classname TeacherController
 * @Description TODO
 * @Created by cxk
 */
@RestController
@RequestMapping("/teacher")
@RequireToken
@Api(tags = "教师接口")
public class TeacherController {
    @Resource
    private TeacherService teacherService2;

    @GetMapping("/course")
    @ApiOperation(value = "获取教师课程",notes = "用于：教师课程查询，输入一个或多个属性参数，也可以传入分页参数")
    public Msg findCourse(TokenVO tokenVO,Pageable p){
        return MsgUtil.success("课程获取成功",teacherService2.findCourseByUid(tokenVO.getUid(), p));
    }
}
