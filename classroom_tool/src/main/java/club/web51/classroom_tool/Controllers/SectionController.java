package club.web51.classroominteractiontoolsadmin.Controllers;

import club.web51.classroominteractiontoolsadmin.DO.MainDO.Section;
import club.web51.classroominteractiontoolsadmin.DO.SystemDO.Msg;
import club.web51.classroominteractiontoolsadmin.Services.SectionService;
import club.web51.classroominteractiontoolsadmin.Utils.MsgUtil;
import club.web51.classroominteractiontoolsadmin.Utils.ObjectUtil;
import club.web51.classroominteractiontoolsadmin.VO.TokenVO;
import club.web51.classroominteractiontoolsadmin.Security.RequireToken;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Pageable;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 服务类
 * @author HanYuHao
 * @since 1.0.0
 */
@RestController
@RequireToken
@RequestMapping("/section")
@Api(tags = "群组相关接口")
public class SectionController {

    @Resource
    SectionService sectionService;



    /**
     * 根据Group的字段,自动生成条件,字段的值为null不生成条件
     * http://localhost:8080/user/?id=1
     * @param section 实体对象
     * @param pageable 分页/排序对象
     * @return 返回的是实体,里面涵盖分页信息及状态码
     */
     @GetMapping
     @ApiOperation(value = "查找群组",notes = "用于：群组查询，输入一个或多个属性参数，也可以传入分页参数")
     Msg search(Section section, Pageable pageable) {
             return sectionService.search(section, pageable);
     }
     @PostMapping
     @ApiOperation(value = "创建群组",notes = "用于：群组创建")
     Msg create(@RequestBody @Valid Section section, BindingResult bindingResult, TokenVO tokenVO) {
         if(bindingResult.hasErrors()){
             return MsgUtil.fail(String.valueOf(ObjectUtil.NonNullDefault(bindingResult.getFieldError().getDefaultMessage(),"发生了错误")));
         }
         //设置群组管理员ID为登录的用户
         section.setUid(tokenVO.getUid());
         return sectionService.create(section);
     }
     @PutMapping
     @ApiOperation(value = "更新群组",notes = "用于：群组更新，输入一个或多个属性参数修改，ID必传")
     Msg update(@RequestBody Section section){
         return sectionService.update(section);
     }
     @DeleteMapping("/")
     @ApiOperation(value = "删除群组",notes = "用于：群组删除，传入ID")
     Msg delete(Long id){
         return sectionService.delete(id);
     }
}
