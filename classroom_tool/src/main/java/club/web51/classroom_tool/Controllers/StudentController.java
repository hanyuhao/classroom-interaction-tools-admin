package club.web51.classroominteractiontoolsadmin.Controllers;

import club.web51.classroominteractiontoolsadmin.DO.SystemDO.Msg;
import club.web51.classroominteractiontoolsadmin.Services.StudentService;
import club.web51.classroominteractiontoolsadmin.Utils.MsgUtil;
import club.web51.classroominteractiontoolsadmin.VO.TokenVO;
import club.web51.classroominteractiontoolsadmin.Security.RequireToken;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * @Classname StudentController
 * @Description TODO
 * @Date 2020/11/13 23:45
 * @Created by wlh
 */
@RestController()
@RequestMapping("student")
@RequireToken
@Api(tags = "学生相关接口")
public class StudentController {
    @Resource
    private StudentService studentService;

    @GetMapping("/course")
    @ApiOperation(value = "查询学生课程",notes = "用于：学生的课程查询，输入一个或多个属性参数，也可以传入分页参数")
    public Msg get_course(TokenVO tokenVO, Pageable pageable){
        return MsgUtil.success("课程数据获取成功",studentService.findCourseByUid(tokenVO.getUid(), pageable));
    }

    @GetMapping("/section")
    @ApiOperation(value = "查询学生群组",notes = "用于：学生的群组查询，输入一个或多个属性参数，也可以传入分页参数")
    public Msg get_section(TokenVO tokenVO,Pageable pageable){
        return MsgUtil.success("群组数据获取成功",studentService.findSectionByUid(tokenVO.getUid(),pageable));
    }

}
