package club.web51.classroominteractiontoolsadmin.Controllers;

import club.web51.classroominteractiontoolsadmin.DO.SystemDO.Msg;
import club.web51.classroominteractiontoolsadmin.Services.SignDataAddService;
import club.web51.classroominteractiontoolsadmin.Utils.MsgUtil;
import club.web51.classroominteractiontoolsadmin.Security.RequireToken;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Classname SignDataController
 * @Description TODO
 * @Date 2020/11/16 22:16
 * @Created by HanYuHao
 */
@RestController
@RequestMapping("sign/data")
@Api(tags = "签到数据接口")
@RequireToken
public class SignDataController {
    @Resource
    SignDataAddService signDataAddService;

    @PostMapping("add/section")
    @ApiOperation(value = "添加签到学生（按组）",notes = "用于：添加签到学生，输入课程ID，群组ID和签到条目ID，注意不用JSON直接传参")
    public Msg addbysection(@ApiParam(value = "课程ID") Long cid, @ApiParam(value = "群组ID")Long seid,@ApiParam(value = "签到条目ID")String siid){
        signDataAddService.addStudent(cid, seid, siid);
        return MsgUtil.success("添加签到数据成功");
    }


    @PostMapping("add/single")
    @ApiOperation(value = "添加签到学生（单个）",notes = "用于：添加签到学生,输入课程ID，签到条目ID，用户ID，注意不用JSON直接传参")
    public Msg addbysingle(@ApiParam(value = "课程ID") Long cid,@ApiParam(value = "签到条目ID")String siid,@ApiParam(value = "用户ID")Long uid){
        signDataAddService.addStudent(cid, siid, uid);
        return MsgUtil.success("添加签到数据成功");
    }
}
