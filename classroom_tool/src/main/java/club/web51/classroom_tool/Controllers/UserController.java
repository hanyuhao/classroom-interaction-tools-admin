package club.web51.classroominteractiontoolsadmin.Controllers;

import club.web51.classroominteractiontoolsadmin.DO.SystemDO.Msg;
import club.web51.classroominteractiontoolsadmin.DTO.UserInfoDTO;
import club.web51.classroominteractiontoolsadmin.Utils.MsgUtil;
import club.web51.classroominteractiontoolsadmin.Utils.ObjectUtil;
import club.web51.classroominteractiontoolsadmin.VO.TokenVO;
import club.web51.classroominteractiontoolsadmin.VO.UserVO;
import club.web51.classroominteractiontoolsadmin.Security.JwtUtil;
import club.web51.classroominteractiontoolsadmin.Security.NonRequireToken;
import club.web51.classroominteractiontoolsadmin.Security.RequireToken;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Pageable;
import club.web51.classroominteractiontoolsadmin.DO.MainDO.User;
import club.web51.classroominteractiontoolsadmin.Services.UserService;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 服务类
 * @author HanYuHao
 * @since 1.0.0
 */
@RestController
@RequireToken
@RequestMapping("/user")
@Api(tags = "用户相关接口")
public class UserController {

    final
    UserService userService;
    @Resource
    JwtUtil jwtUtil;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * 根据User的字段,自动生成条件,字段的值为null不生成条件
     * http://localhost:8080/user/?id=1
     * @param user 实体对象
     * @param pageable 分页/排序对象
     * @return 返回的是实体,里面涵盖分页信息及状态码
     */
     @GetMapping
     @ApiOperation(value = "查找所有用户")
     Msg search(User user, Pageable pageable) {
             return MsgUtil.success("获取用户列表成功",userService.search(user, pageable));
     }

    @ApiOperation(value = "查询登录的用户信息",notes = "用于：已经登陆的用户信息获取")
     @GetMapping("/info")
     Msg info(TokenVO tokenVO){
         User user = new User();
         user.setUid(tokenVO.getUid());
         Page<User> users = userService.search(user,Pageable.unpaged());
         User user1 = users.getContent().get(0);
         return MsgUtil.success("用户信息获取成功",new UserInfoDTO(user1));
     }
     @PostMapping
     @ApiOperation(value = "创建用户")
     Msg create(@RequestBody @Valid User user, BindingResult bindingResult) {
         if(bindingResult.hasErrors()){
             return MsgUtil.fail(String.valueOf(ObjectUtil.NonNullDefault(bindingResult.getFieldError().getDefaultMessage(),"发生了错误")));
         }
         return  userService.create(user);
     }
     @PutMapping
     @ApiOperation(value = "修改用户")
     Msg update(@RequestBody User user){
         return userService.update(user);
     }
     @DeleteMapping("/")
     @ApiOperation(value = "删除用户")
     Msg delete(Long uid){
         return userService.delete(uid);
     }

     @NonRequireToken
     @PostMapping("/login")
     @ApiOperation(value = "用户登录")
    Msg login(@RequestBody UserVO user){
         System.out.println(user);
         User user1 = User.builder().uid(user.getUid()).password(user.getPassword()).build();
         userService.login(user1);
         if(userService.search(user1,Pageable.unpaged()).getContent().isEmpty()){
             return MsgUtil.fail("登录失败");
         }
         User user2 = userService.search(user1,Pageable.unpaged()).getContent().get(0);
         String token = jwtUtil.sign(user2);
         return MsgUtil.success("登录成功",UserVO.builder().uid(user.getUid()).password(user.getPassword()).token(token).build());
     }

    @ApiOperation(value = "用户登出")
     @PostMapping("/logout")
    Msg logout(){

         return MsgUtil.success("退出成功");
     }
}
