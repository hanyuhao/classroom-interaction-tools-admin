package club.web51.classroominteractiontoolsadmin.Controllers;

import club.web51.classroominteractiontoolsadmin.DO.SystemDO.Msg;
import club.web51.classroominteractiontoolsadmin.Utils.MsgUtil;
import club.web51.classroominteractiontoolsadmin.Utils.ObjectUtil;
import club.web51.classroominteractiontoolsadmin.Security.RequireToken;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Pageable;
import club.web51.classroominteractiontoolsadmin.DO.MainDO.Sign;
import club.web51.classroominteractiontoolsadmin.Services.SignService;

import javax.validation.Valid;

/**
 * 服务类
 * @author HanYuHao
 * @since 1.0.0
 */
@RestController
@RequireToken
@RequestMapping("/sign")
@Api(tags = "签到条目相关接口")
public class SignController {

    final
    SignService signService;

    public SignController(SignService signService) {
        this.signService = signService;
    }

    /**
     * 根据Sign的字段,自动生成条件,字段的值为null不生成条件
     * http://localhost:8080/user/?id=1
     * @param sign 实体对象
     * @param pageable 分页/排序对象
     * @return 返回的是实体,里面涵盖分页信息及状态码
     */
     @GetMapping
     @ApiOperation(value = "查找签到条目",notes = "用于：签到条目查询，输入一个或多个属性参数，也可以传入分页参数")
     Msg search(Sign sign, Pageable pageable) {
             return signService.search(sign, pageable);
     }
     @PostMapping
     @ApiOperation(value = "发起一个签到")
     Msg creat(@RequestBody @Valid Sign sign, BindingResult bindingResult) {
         if(bindingResult.hasErrors()){
             return MsgUtil.fail(String.valueOf(ObjectUtil.NonNullDefault(bindingResult.getFieldError().getDefaultMessage(),"发生了错误")));
         }
         return signService.create(sign);
     }
     @PutMapping
     @ApiOperation(value = "修改一个发起的签到")
     Msg update(@RequestBody  Sign sign){
         return signService.update(sign);
     }
     @DeleteMapping("/")
     @ApiOperation(value = "删除这个签到",notes = "用于：签到删除，传入ID即可")
     Msg delete(String id){
         return signService.delete(id);
     }
}
