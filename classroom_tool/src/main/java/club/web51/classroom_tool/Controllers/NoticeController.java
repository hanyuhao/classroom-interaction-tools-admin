package club.web51.classroominteractiontoolsadmin.Controllers;

import club.web51.classroominteractiontoolsadmin.DO.MainDO.Notice;
import club.web51.classroominteractiontoolsadmin.DO.SystemDO.Msg;
import club.web51.classroominteractiontoolsadmin.Enums.MsgEnum;
import club.web51.classroominteractiontoolsadmin.Exceptions.ErrDataException;
import club.web51.classroominteractiontoolsadmin.Security.RequireToken;
import club.web51.classroominteractiontoolsadmin.Services.NoticeService;
import club.web51.classroominteractiontoolsadmin.Utils.MsgUtil;
import club.web51.classroominteractiontoolsadmin.Utils.ObjectUtil;
import club.web51.classroominteractiontoolsadmin.VO.NoticeVO;
import club.web51.classroominteractiontoolsadmin.VO.TokenVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @Classname NoticeController
 * @Description TODO 公告控制器
 * @Date 2020/11/22 0:49
 * @Created by wlh
 */
@RestController
@Api(tags = "公告相关接口")
@RequestMapping("notice")
@RequireToken
public class NoticeController {
    @Resource
    private NoticeService noticeService;

    @GetMapping("")
    @ApiOperation(value = "查询公告",notes = "用于：根据传入的属性值查询公告，不传入属性则查询所有；查询 系统公告 使得isSystem是true即可")
    public Msg findAll(NoticeVO noticeVO,Pageable pageable){

        Notice notice = (Notice) ObjectUtil.NonNullDefault(noticeVO.getNotice(),Notice.builder().build());
        if(Objects.isNull(noticeVO.getBindCourse())){
            noticeVO.setBindCourse(false);
        }
        if(!noticeVO.getBindCourse())
        return MsgUtil.success("公告获取成功",noticeService.findAll(notice,pageable));
        else{
            return MsgUtil.success("课程公告获取成功",noticeService.findByCourse(noticeVO.getCid(),notice,pageable));
        }
    }

    @PostMapping("")
    @ApiOperation(value = "增加公告",notes = "用于：增加一条公告，可用于系统公告或专属课程公告")
    public Msg addOne(@RequestBody NoticeVO noticeVO, TokenVO tokenVO){

        Notice notice = noticeVO.getNotice();
        //设置作者
        notice.setUid(tokenVO.getUid());
        //绑定
        if(Objects.isNull(noticeVO.getBindCourse())){
            noticeVO.setBindCourse(false);
            if(Objects.isNull(noticeVO.getCid())){
                throw new ErrDataException(MsgEnum.ERROR_DATA_EMPTY);
            }
        }

        if(noticeVO.getBindCourse()){
            //绑定课程处理
            noticeService.addOne(notice,noticeVO.getCid());
        }else{
            //不绑定处理
            noticeService.addOne(notice);
        }
        return MsgUtil.success("公告增加成功",noticeService);
    }

    @PutMapping("")
    @ApiOperation(value = "修改公告",notes = "修改公告一个或多个属性，不为null的属性值都会被修改")
    public Msg updateOne(@RequestBody Notice notice){
        noticeService.updateOne(notice);
        return MsgUtil.success("修改成功");
    }

    @DeleteMapping("")
    @ApiOperation(value = "删除公告",notes = "用于：删除公告")
    public Msg deleteOne(Long id){
        noticeService.deleteOne(id);
        return MsgUtil.success("删除成功");
    }


}
