package club.web51.classroominteractiontoolsadmin.Controllers;

import club.web51.classroominteractiontoolsadmin.DO.SystemDO.Msg;
import club.web51.classroominteractiontoolsadmin.Security.NonRequireToken;
import club.web51.classroominteractiontoolsadmin.Security.RequireToken;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Pageable;
import club.web51.classroominteractiontoolsadmin.DO.MainDO.Setting;
import club.web51.classroominteractiontoolsadmin.Services.SettingService;
/**
 * 服务类
 * @author HanYuHao
 * @since 1.0.0
 */
@RestController
@RequireToken
@RequestMapping("/setting")
@Api(tags = "系统设置相关接口")
public class SettingController {

    final
    SettingService settingService;

    public SettingController(SettingService settingService) {
        this.settingService = settingService;
    }

    /**
     * 根据Setting的字段,自动生成条件,字段的值为null不生成条件
     * http://localhost:8080/user/?id=1
     * @param setting 实体对象
     * @param pageable 分页/排序对象
     * @return 返回的是实体,里面涵盖分页信息及状态码
     */
     @GetMapping
     @NonRequireToken
     @ApiOperation(value = "查找设置",notes = "用于：根据名字查找设置值")
     Msg search(Setting setting, Pageable pageable) {
             return settingService.search(setting, pageable);
     }
//     @PostMapping
//     void create() {
//     }
//     @PutMapping
//     void update(){
//     }
//     @DeleteMapping("/")
//     void delete(){
//     }
}
