package club.web51.classroominteractiontoolsadmin.Utils;

import club.web51.classroominteractiontoolsadmin.DO.SystemDO.Msg;
import club.web51.classroominteractiontoolsadmin.Enums.MsgEnum;
import lombok.experimental.UtilityClass;

import java.util.Objects;

/**
 * @Classname MsgUtil
 * @Description TODO
 * @Date 2020/11/24 16:11
 * @Created by wxt
 */
@UtilityClass
public class MsgUtil {
    public Msg success(){
        return Msg.builder().code(0).description("成功").build();
    }

    public Msg success(String msg){
        return Msg.builder().code(0).description(msg).build();
    }

    public Msg success(String msg, Object data){
        return Msg.builder().code(0).description(msg).data(data).build();
    }

    public Msg fail(){
        return Msg.builder().code(-1).description("失败").build();
    }

    public Msg fail(String msg){
        return Msg.builder().code(-1).description(msg).build();
    }

    public Msg fail(String msg,Object data){
        return Msg.builder().code(-1).description(msg).data(data).build();
    }

    public Msg get(MsgEnum msgEnum){
        return Msg.builder().code(msgEnum.getCode()).description(msgEnum.getDescription()).build();
    }
}
