package club.web51.classroominteractiontoolsadmin.Utils;

import java.util.Objects;

/**
 * @Classname ObjectUtil
 * @Description TODO
 * @Date 2020/11/24 16:20
 * @Created by wxt
 */
public class ObjectUtil {

    /**
     * 返回不为空的对象，若对象为空用后者替代
     * @param obj 返回对象
     * @param def_obj 替代返回对象
     * @return 不为空的返回对象
     */
    public static Object NonNullDefault(Object obj,Object def_obj){
        if(Objects.isNull(obj) && Objects.isNull(def_obj)){
            throw new IllegalArgumentException("参数不能全为Null");
        }
        if(Objects.isNull(obj)){
            return def_obj;
        }else{
            return obj;
        }
    }
}
